const express = require("express");

const app = express();

let port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));


let users = [
    {
        "username": "johndoe",
        "password": "johndoe1234"
    },
    {
        "username": "janesmith",
        "password": "janesmith1234"
    }
];
console.log(users);

app.get("/home", (request, response) => {
    response.send(`Welcome to home page`)
});

app.get("/users", (request, response) => {
    response.send(users)
});

app.delete("/delete-user", (request, response) => {
    
    let notif;

    for(let i = 0; i < users.length; i++){
        if(request.body.username == users[i].username && request.body.password == users[i].password){
            users.splice(i,1);

            notif = `User ${request.body.username} has been deleted.`
            console.log(users);
            break;
        } else {
            notif = `User does not exist`
        }
    };

    response.send(notif);
 
});


app.listen(port, ()=> console.log(`Server running at port ${port}`));